# telegraf-config

* [About telegraf-config](#about-telegraf-config)
* [Deploying](#deploying)
  * [With tmux](#with-tmux)
  * [Without tmux](#without-tmux)
  * [Without any third-party tools](#without-tmux-and-without-third-party-tools)

# About telegraf-config

This is my default [telegraf](https://github.com/influxdata/telegraf) config for my [TrueNAS Scale](https://www.truenas.com/download-truenas-scale/) server for use with [Aria Operations for Applications](https://www.vmware.com/products/aria-operations-for-applications.html)

# Deploying

I try to install as few packages as possible since you should avoid third party applications on your NAS.  Because of this I run the standalone binary in a tmux session or you can run this in the background without tmux

## With [tmux](https://github.com/tmux/tmux/wiki)

```
#> tmux
#> wget https://dl.influxdata.com/telegraf/releases/telegraf-1.25.2_linux_amd64.tar.gz
#> tar xvf telegraf-1.25.2_linux_amd64.tar.gz
#> git clone https://gitlab.com/krisclarkdev/telegraf-config.git
#> telegraf-1.25.2/usr/bin/telegraf --config telegraf-config/telegraf.conf
```

## Without [tmux](https://github.com/tmux/tmux/wiki)

```
#> wget https://dl.influxdata.com/telegraf/releases/telegraf-1.25.2_linux_amd64.tar.gz
#> tar xvf telegraf-1.25.2_linux_amd64.tar.gz
#> git clone https://gitlab.com/krisclarkdev/telegraf-config.git
#> telegraf-1.25.2/usr/bin/telegraf --config telegraf-config/telegraf.conf &
```

## Without [tmux](https://github.com/tmux/tmux/wiki) and without third-party tools


From local machine
```
$> wget https://dl.influxdata.com/telegraf/releases/telegraf-1.25.2_linux_amd64.tar.gz
$> tar xvf telegraf-1.25.2_linux_amd64.tar.gz
$> scp -r ./telegraf-1.25 user@host
$> git clone https://gitlab.com/krisclarkdev/telegraf-config.git
$> scp -r ./telegraf-config user@host:~/
```

From server
```
#> telegraf-1.25.2/usr/bin/telegraf --config telegraf-config/telegraf.conf &
```
